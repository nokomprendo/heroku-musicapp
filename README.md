
# Déploiement d'une application Haskell/Postgresql sur Heroku

- installation : heroku-cli, nodejs, postqresql

- déploiement :

```
heroku login
heroku create --buildpack https://github.com/mfine/heroku-buildpack-stack.git
git push heroku master
heroku addons:create heroku-postgresql:hobby-dev
heroku psql -f music.sql
heroku logout
```

- execution locale :

```
heroku login
PORT=3000 DATABASE_URL=`heroku config:get DATABASE_URL` nix-shell --run "cabal run"
```

